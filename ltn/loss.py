import tensorflow as tf


def ltn_loss(y_true, y_pred):

    return tf.reduce_sum(input_tensor=y_pred, keepdims=True)

def smooth(parameters, default_smooth_factor):
    norm_of_omega = tf.reduce_sum(tf.expand_dims(tf.concat(
        [tf.expand_dims(tf.reduce_sum(tf.square(par)), 0) for par in parameters], axis=0), 1))
    return tf.multiply(default_smooth_factor, norm_of_omega)