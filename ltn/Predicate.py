from keras import backend as K
from keras.layers import Layer, Activation
from keras.layers.merge import concatenate
from tensorflow.contrib.layers.python.layers.regularizers import l2_regularizer
import tensorflow as tf



class Predicate(Layer):
    def __init__(self, num_features, k,i,  **kwargs):
        super(Predicate, self).__init__(**kwargs)
        self.output_dim = 1
        self.num_features = num_features
        self.k = k
        self.name = 'predicate_{}'.format(i)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        mn = self.num_features
        self.up = self.add_weight(name='up',  # 1 x k
                                  shape=(self.k, 1),
                                  initializer='random_normal',
                                  trainable=True)

        self.Wp = self.add_weight(name='Wp',
                                  shape=(self.k, mn, mn),
                                  initializer='random_normal',
                                  trainable=True)
        self.Vp = self.add_weight(name='Vp',
                                  shape=(self.k, mn),
                                  initializer='random_normal',
                                  trainable=True)
        self.bp = self.add_weight(name='bp',
                                  shape=(1, self.k),
                                  initializer='random_normal',
                                  trainable=True)

        super(Predicate, self).build(input_shape)  # Be sure to call this at the end

    def call(self, x):
        #with tf.Session() as sess:  print(c.eval())

        # (1,batch,features) -> (batch,features)
        #x = tf.Print(x,[x],"x:")
        X = tf.squeeze(x)
        #X = tf.Print(X,[X],"X:")
        # X = x
        XW = tf.matmul(tf.tile(tf.expand_dims(X, 0), [self.k, 1, 1]), self.Wp)
        #        XW = tf.Print(XW,[XW],"XW:")
        XWX = tf.squeeze(tf.matmul(tf.expand_dims(X, 1), tf.transpose(a=XW, perm=[1, 2, 0])))
        #       XWX = tf.Print(XWX,[XWX],"XWX:")
        XV = tf.matmul(X, tf.transpose(a=self.Vp))
        #      XV = tf.Print(XV,[XV],"XV:")
        gX = tf.matmul(tf.tanh(XWX + XV + self.bp), self.up)
        #     gX = tf.Print(gX,[gX],"gX:")
        h = tf.sigmoid(gX)
        # h = tf.Print(h,[h],"h{}:".format(self.name))
        return h

