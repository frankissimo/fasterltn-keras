import keras
import tensorflow as tf
from keras.layers import Layer, Activation

eps = 1e-4
not_zeros = lambda x: (1 - eps) * x + eps
not_ones = lambda x: (1 - eps) * x

class Wrapper_Quantifier:
    def __init__(self, aggreg_op):
        self._aggreg_op = aggreg_op

    def __call__(self, variable):
        return keras.layers.Lambda(lambda o: self._aggreg_op(o))(variable)



class Not_std:
    def __call__(self, x):
        return keras.layers.Lambda(lambda o: 1 - o)(x)

class And_Min:
    def __call__(self, x):
        return tf.reduce_min(x,0)

class And_Luk:
    def __call__(self, x):
        return tf.maximum(tf.reduce_sum(input_tensor=x,  keepdims=True) - x.shape.dims[0],0)

class Or_Max:
    def __call__(self, x):
        return tf.reduce_max(x,0)

class Or_Luk:
    def __call__(self, x):
        return tf.minimum(1.0, tf.reduce_sum(x,1 , keepdims=True))

class Implies_Luk:
    def __call__(self,x):
        return tf.minimum(1.-tf.reduce_sum(x),1.)

class Aggreg_Min:
    def __call__(self,xs,axis=None,keepdims=True):
        return tf.reduce_min(xs,axis=axis,keepdims=keepdims)
class Aggreg_Max:
    def __call__(self,xs,axis=None,keepdims=True):
        return tf.reduce_max(xs,axis=axis,keepdims=keepdims)
class Aggreg_Mean:
    def __call__(self,xs,axis=None,keepdims=True):
        return tf.reduce_mean(xs,keepdims=keepdims)

class Aggreg_pMean:
    def __call__(self,xs,p=2,axis=None,keepdims=True,stable=None):
        return tf.pow(tf.reduce_mean(tf.pow(xs,p),axis=axis,keepdims=keepdims),1/p)
class Aggreg_pMeanError:
    def __init__(self,p=2,stable=True):
        self.p = p
        self.stable = stable
    def __call__(self,xs,axis=None,keepdims=False,stable=None):
        p = self.p if p is None else p
        stable = self.stable if stable is None else stable
        if stable:
            xs = not_ones(xs)
        return 1.-tf.pow(tf.reduce_mean(tf.pow(1.-xs,p),axis=axis,keepdims=keepdims),1/p)
class logsum:
    def __call__(self, xs, axis=None, keepdims=False):
        return tf.negative(tf.reduce_sum(input_tensor=tf.math.log(xs), keepdims=True))



"""
class Clause(Layer):

    def __init__(self, num_classes, aggregator=None, **kwargs):
        super(Clause, self).__init__(**kwargs)

        self.num_classes = num_classes
        self.aggregator = aggregator

    def build(self, input_shape):
        super(Clause, self).build(input_shape)

    def compute_output_shape(self, inputShape):
        return [(1, 1)]

    def call(self, input, mask=None):
        if self.aggregator == "product":
            h = tf.reduce_prod(input_tensor=input, keepdims=True)
        if self.aggregator == "mean":
            h = tf.reduce_mean(input_tensor=input, keepdims=True)
        if self.aggregator == "gmean":
            h = tf.exp(tf.mul(tf.reduce_sum(input_tensor=tf.math.log(input), keepdims=True),
                              tf.inv(tf.cast(tf.size(input=input), dtype=tf.float32))))
        if self.aggregator == "hmean":
            h = tf.compat.v1.div(tf.cast(tf.size(input=input), dtype=tf.float32),
                                 tf.reduce_sum(input_tensor=tf.math.reciprocal(input), keepdims=True))
            return h
        if self.aggregator == "min":
            h = tf.reduce_min(input_tensor=input, keepdims=True)

        if self.aggregator == "logsum":
            h = tf.negative(tf.reduce_sum(input_tensor=tf.math.log(input), keepdims=True, name="Clause_" + self.name))

        if self.aggregator == "focal_loss_logsum":
            fl = tf.math.multiply(tf.math.pow((1 - input), self.gamma), tf.math.log(input))
            if self.alpha:
                fl = tf.negative(self.alpha * fl)
            else:
                fl = tf.negative(fl)
            h = tf.reduce_sum(input_tensor=fl, keepdims=True, name="Clause_" + self.num_classes)

        # out_ltn = keras.layers.Concatenate(axis=1)(h)
        # out_ltn = keras.layers.Lambda(lambda x: keras.backend.expand_dims(x, 0))(h)
        return h
"""