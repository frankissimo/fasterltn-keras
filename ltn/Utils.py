import tensorflow as tf
from keras.layers import Layer, Activation
import csv

from ltn.Clause import Clause
from ltn.Literal import Literal_O, Literal
from ltn.Predicate import Predicate
from ltn.bb_creation import bb_creation
from pascalpart import containment_ratios_between_two_bbxes


class Pair(Layer):
    '''
    Pair Layers allow to identificate all pairs between features extracted,
    including a containment ratio between the bounding box represented
    Return a series of tensor composed by the input data plus the containment ratio value
    '''

    def __init__(self, batch_size, **kwargs):
        super(Pair, self).__init__(**kwargs)
        self.batch_size = batch_size

    def build(self, input_shape):
        super(Pair, self).build(input_shape)

    def call(self, inputs, **kwargs):
        outputs = []
        for i in range(self.batch_size // 2):
            for j in range(self.batch_size // 2):
                cts = containment_ratios_between_two_bbxes(inputs[0, i, :], inputs[0, j, :])
                x = tf.concat([inputs[0, i, :], inputs[0, j, :], cts], axis=0)
                x = tf.expand_dims(tf.expand_dims(x, axis=0), axis=0)
                outputs.append(x)
        return tf.concat(outputs, axis=1)


def disjunctions_classes(classes, predictions, tnorm, aggregator, gamma):
    '''

    :param classes: labels represented by predicates
    :param predictions: output of linear layer obtained from FASTERCNN composed by num_roi features
    :param tnorm: t-norm function
    :param aggregator: aggregator function
    :param gamma: gamma value for the focal loss function

    '''
    output = []
    for t1 in classes:
        for t in classes:
            if t < t1:
                l1 = Literal_O(False)(predictions[t])
                l2 = Literal_O(False)(predictions[t1])
                x = Clause(tnorm=tnorm, aggregator=aggregator, gamma=gamma, name='disjoint_{}_{}'.format(t, t1))(
                    [l1, l2])

                output.append(x)
    return output


def partOF(nb_classes, num_rois, out_class, input_rois, base_layers, tnorm, aggregator, gamma):
    '''
    PARTOF PREDICATE features are composed by the number of predicates, 4 coordinates element, 1 containment ratio

    :param nb_classes: number of predicates
    :param num_rois: number of roi output fasterCNN
    :param out_class: FASTERCNN linear function output
    :param input_rois: ROI output
    :param base_layers: shared weights
    :param tnorm: t-norm function
    :param aggregator: aggregator function
    :param gamma: gamma value for the focal loss function

    '''
    output = []
    x = bb_creation(nb_classes, num_rois)([out_class, input_rois, base_layers])
    x = Pair(num_rois)(x)
    partOf = Predicate(num_features=(nb_classes + 5) * 2, k=6, i=nb_classes + 1)
    partOf_prediction = partOf(x)
    x = Literal(name='partOf_literal', batch_size=num_rois // 2 * num_rois // 2)([partOf_prediction, Y_partOf])
    x = Clause(tnorm=tnorm, aggregator=aggregator, gamma=gamma, name='partOf')(x)
    output.append(x)
    return output


def at_least_one_class(classes, predictions, tnorm, aggregator, gamma):
    '''
    Logical axioms to force the membership of a bounding box to at least one class

    :param classes: predicate classes vector
    :param predictions: predictions of FASTERCNN linear layer
    :param tnorm: t-norm function
    :param aggregator: aggregator function
    :param gamma: gamma value for the focal loss function

    '''
    output = []
    at_least_literals = []
    for t in classes:
        l = Literal_O(True)(predictions[t])
        at_least_literals.append(l)
    x = Clause(tnorm=tnorm, aggregator=aggregator, gamma=gamma, name='at_least_one_class')(at_least_literals)
    output.append(x)
    return output


def parts_of_wholes(parts_of_whole, wholes, partOf_prediction, parts, tnorm, aggregator, gamma):
    '''
        Logical axioms to express the relationship between parts and whole object including the rule
        that a part cannot be included in another part

        :param classes: predicate classes vector
        :param predictions: predictions of FASTERCNN linear layer
        :param tnorm: t-norm function
        :param aggregator: aggregator function
        :param gamma: gamma value for the focal loss function

        '''
    output = []
    for w in parts_of_whole.keys():
        l0 = Literal_O(False)(wholes[w])
        l1 = Literal_O(False)(partOf_prediction)
        literals = [l0, l1]
        for p in parts_of_whole[w]:
            l = Literal_O(True)(parts[p])
            literals.append(l)
        x = Clause(tnorm=tnorm, aggregator=aggregator, gamma=gamma, name='parts_of_wholes_' + w)(literals)
        output.append(x)
    return output


def wholes_of_parts(wholes_of_part, wholes, partOf_prediction, parts, tnorm, aggregator, gamma):
    '''
        Logical axioms to express the relationship between parts and whole object including the rule
        that a whole cannot be included in another whole

        :param classes: predicate classes vector
        :param predictions: predictions of FASTERCNN linear layer
        :param tnorm: t-norm function
        :param aggregator: aggregator function
        :param gamma: gamma value for the focal loss function

        '''
    output = []
    # wholes of parts
    for p in wholes_of_part.keys():
        l0 = Literal_O(False)(parts[p])
        l1 = Literal_O(False)(partOf_prediction)
        literals = [l0, l1]
        for w in wholes_of_part[p]:
            l = Literal_O(True)(wholes[w])
            literals.append(l)
        x = Clause(tnorm=tnorm, aggregator=aggregator, gamma=gamma, name='wholes_of_parts_' + p)(literals)
        output.append(x)
    return output
