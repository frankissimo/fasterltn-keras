# KERAS IMPLEMENTATION OF LOGIC TENSOR NETWORKS FOR SEMANTIC IMAGE INTERPRETATION

- This repository contains an implementation of Logic Tensor Network for Semantic Image Interpretation, implicated into 'Faster-LTN: a neuro-symbolic, end-to-end object
    detection architecture'. The LTN block is included in FASTERCNN training with end-to-end framework. 
- The code based on LTN keras Library https://keras.io/ with Python 3.7 on Windows system.

